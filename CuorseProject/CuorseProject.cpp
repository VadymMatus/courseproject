﻿#include "pch.h"

using namespace std;

/**
 * Функция высчитывает количетво записей структуры в бинарном файле.
 *
 * @param FileName Имя файла
 * @param Object Обьект струтуры, количество записей которой, нужно посчитать
 * в бинарном файле.
 *
 * @return Количество записей структуры в бинарном файле
 */
const size_t num_entries_file()
{
	ifstream ifs(FILE_NAME, std::ios_base::binary | std::ios_base::ate /*открытие с конца*/);
	size_t temp = ifs.tellg() / sizeof(Balls);
	ifs.close();
	return temp;
}

/**
 * Функция создаёт новый файл.
 * Если файл существует - будет выведенно сообщение,
 * з вопросом о его перезаписи
 */
void create_new_file();

/**
 * Функция добавления новой записи в файл.
 */
void add_record();

/**
 * Функция проверяет файл на пустоту.
 *
 * @param std::string Имя файла
 * @return <tt>true</tt> - файл пустой, <tt>false</tt> - файл не пустой
 */
bool file_is_empty(std::string);

/**
 * Функция подсчёта длины числа.
 *
 * @param num Число для подсчёта его длины
 * @return int - Длина числа
 */
const size_t size_number(float& num)
{
	std::stringstream ss;

	ss << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << num;

	return ss.str().size();
}

/**
 * Функция подсчёта длины числа.
 *
 * @param num Число для подсчёта его длины
 * @return int - Длина числа
 */
const size_t size_number(int num)
{
	std::stringstream ss;

	ss << num;

	return ss.str().size();
}

/**
 * Функция проверяет файл на существование.
 *
 * @param std::string Имя файла
 * @return <tt>true</tt> - файл существует, <tt>false</tt> - файл не существует
 */
bool file_is_exists(std::string);

/**
 * Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @param value Переменая для ввода
 * @param message Сообщение в случае ошибки
 */
void input_check(int &value, const char *message)
{
	while (!(std::cin >> value))
	{
		std::cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << message;
	}

	cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * функция ввода и проверки коректности ввода.
 *  являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @param value переменая для ввода
 * @param message сообщение в случае ошибки
 */
void input_check(float &value, const char *message)
{
	while (!(std::cin >> value))
	{
		std::cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << message;
	}

	cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * опрос пользователя (y/n) ...
 *
 * @return <tt>true</tt> - согласился, <tt>false</tt> - не согласился
 */
bool answer_check()
{
	const char answer(cin.get()); // ввод ответа
	cin.unget();
	cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

	return (strchr("y|Y|д|Д|" /*ответы которые показывают согласие*/, answer) != NULL);
}

/**
 * функция просмотра всех записей в файле.
 * вывод таблицы с всемя записями в файле, без действий.
 */
void show_all_records();

/**
 * Функция редактирования записей в файле.
 * Выполняеться поиск записи в файле,
 * и предоставлене выбора поля записи для редактирования.
 * Редактирование как отдельного поля, так и всей записи целеком.
 */
void edit_record();

/**
 * Функция удаления записей в файле.
 * Поиск удаляемой записи в файле и удаление после подтверждения пользователя.
 */
void delete_record();

/**
 * Функция сортировки записей в файле.
 * Сортировка записей в файле по имени и цене за еденицу тавара.
 * Сортировка выполняеться как по возростанию, так и по убыванию.
 */
void sorting_records();

/**
 * Функция выполнения запросов.
 * Пользователя предоставляеться выбор возможного запроса,
 * после чего обрабатываються записи в файле, и выдаёться ответ.
 */
void request();

/**
 * Функция поиска записей в файле.
 * Поиск в файле искомой записи. Если в файле нету записи
 * с таким именем (с учётом реестра),
 * тогда выполняеться поиск без учёта реестра.
 */
void find();

/**
 * Функция переобразования всей стоки в нижний реестр.
 *
 * @param str Целевая строка
 *
 * @return char* Строка в нижнем реестре
 */
char* str_tolower(const char* str)
{
	char *tmp = new char[strlen(str) + 1];

	for (int i(0); i < strlen(str) + 1; i++)
		tmp[i] = tolower(str[i]);

	return tmp;
}

#include <Windows.h>

int main() {
	SetConsoleOutputCP(CP_UTF8);

	int answer(0);

	do
	{
		system("cls");
		cout << setw(15) << u8"Меню" << endl
			<< u8"1. Створення нового файлу" << endl
			<< u8"2. Перегляд файлу проданих м'ячiв" << endl
			<< u8"3. Додавання записiв" << endl
			<< u8"4. Редагування записiв" << endl
			<< u8"5. Видалення записiв" << endl
			<< u8"6. Сортування iнформацiї" << endl
			<< u8"7. Виконання запитiв" << endl
			<< u8"8. Пошук" << endl
			<< u8"9. Вихiд" << endl
			<< u8"Ваш вибiр: ";

		SetConsoleCP(1251);
		input_check(answer, "Error: Ваш вибiр: ");
		SetConsoleOutputCP(CP_UTF8);
		system("cls");

		switch (answer)
		{
		case 1: // створення нового файлу
			create_new_file();
			break;
		case 2: // перегляд файлу проданих м'ячiв
			show_all_records();
			getchar();
			break;
		case 3: // додавання записiв
			add_record();
			break;
		case 4: // редагування записiв
			edit_record();
			break;
		case 5: // видалення записiв
			delete_record();
			break;
		case 6: // сортування iнформацiї
			sorting_records();
			break;
		case 7: // виконання запитiв
			request();
			break;
		case 8: // пошук
			find();
			break;
		case 9: // вихiд
			return EXIT_SUCCESS;
		default:
			break;
		}


	} while (true);
}

void create_new_file()
{
	if (file_is_exists(FILE_NAME) == true)
	{
		cout << u8"Файл уснує! Створити новий [y/N]?: ";
		if (answer_check() == false) return;
	}

	ofstream(FILE_NAME, ios_base::binary | ios_base::out);
	cout << u8"Файл створенно!" << endl;
	getchar();
}

bool file_is_empty(std::string FileName)
{
	return std::ifstream(FileName, std::ios_base::binary | std::ios_base::ate).tellg() /*длина*/ == 0;
}

bool file_is_exists(std::string FileName)
{
	return std::ifstream(FileName, ios_base::_Nocreate | ios_base::in).is_open() /*если открыт*/;
}

void add_record()
{
	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		return;
	}

	Balls balls; // Заполняемая структура

	// Ввод типа мяча
	do
	{
		cout << endl << u8"Вкажiть тип мяча: " << endl
			<< u8"1. Футбольний" << endl
			<< u8"2. Волейбольний" << endl
			<< u8"3. Баскетбольний" << endl
			<< u8"Ваш вибiр: ";
		input_check(balls.type, "Ваш вибiр: ");
	} while (balls.type < FOOTBALL || balls.type > BASKETBALL); // Если не входит в зарезервированые типы

	// Ввод материала мяча
	do
	{
		cout << endl << u8"Вкажiть матерiал м'яча: " << endl
			<< u8"1. Синтетична шкiра" << endl
			<< u8"2. Штучна шкiра" << endl
			<< u8"3. Композитна шкiра" << endl
			<< u8"4. Натуральна шкiра" << endl
			<< u8"Ваш вибiр: ";
		input_check(balls.material, "Ваш вибiр: ");
	} while (balls.material < SYNTHETIC || balls.material > NATURAL); // Если не входит в зарезервированые типы

	// Ввод имени мяча
	do
	{
		cout << endl << u8"Марка м'яча (макс. 10 символiв): ";
		cin.getline(balls.marka, MAX_MARKA_SIZE);
	} while (strlen(balls.marka) == 0); // Марка не может быть пустой

	// Ввод страны производителя мяча
	do
	{
		cout << endl << u8"Вкажiть країну виробника м'яча: " << endl
			<< u8"1. Україна" << endl
			<< u8"2. США" << endl
			<< u8"3. Китай" << endl
			<< u8"4. Японiя" << endl
			<< u8"5. Данiя" << endl
			<< u8"6. Iспанiя" << endl
			<< u8"Ваш вибiр: ";
		input_check(balls.country, "Ваш вибiр: ");
	} while (balls.country < UKRAINE || balls.country > SPAIN); // Если не входит в зарезервированые типы

	// Ввод цены мяча
	do
	{
		cout << endl << u8"Вартiсть м'яча: ";
		input_check(balls.price, "Вартiсть м'яча: ");
	} while (balls.price <= 0); // Цена не может быть меньше 0

	// Ввод количества мячей
	do
	{
		cout << endl << u8"Кiлькiсть м'ячiв: ";
		input_check(balls.count, "Кiлькiсть м'ячiв: ");
	} while (balls.count <= 0); // Количество мячей не может быть меньше 0

	// Общая стоимость мячей
	balls.all_price = balls.price * balls.count;
	//cout << u8"Автоматично вирахувана загальна вартiсть ( " << balls.all_price << u8" )"/*, правильно [y/N]?: "*/;

	//if (answer_check()==false)
	//{
	//	do
	//	{
	//		cout << u8"Загальна вартiсть м'ячiв: ";
	//		input_check(balls.all_price, "Загальна вартiсть м'ячiв: ");
	//	} while (balls.all_price <= 0);
	//}

	std::ofstream ofs(FILE_NAME, ios_base::binary | ios_base::app /*открытие и добавлене в конец файла*/);
	ofs.write((char*)&balls, sizeof(balls));
	ofs.close();

	cout << u8"Додати ще однин запис [y/N]?: ";
	if (answer_check() == true)
	{
		system("cls");
		add_record(); // Рекурсивный вызов ещё одного добвления
	}
}

void show_all_records()
{
	SetConsoleOutputCP(65001);

	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		return;
	}
	else if (file_is_empty(FILE_NAME) == true)
	{
		cout << u8"Файл порожнiй!" << endl;
		return;
	}

	Balls balls;
	int count(1); // Счётчик для первого столбца талбицы

	ifstream ifs(FILE_NAME, ios_base::binary);

	cout << u8"+═════+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
		<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
		<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

	//SetConsoleCP(1251);
	//SetConsoleOutputCP(1251);
	while (ifs.read((char*)&balls, sizeof(balls)))
	{
		// №
		cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << count++ << setw(6 - size_number((count - 1))) << "| "
			// Тип 
			<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : u8"Баскетбольний") << u8" | "
			// Матерiал
			<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : u8"Натуральна шкiра ") << "| "
			// Марка
			<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
			// Країна виробник
			<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : u8"Iспанiя") << u8"         | "
			// Вартiсть за одиницю
			<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
			// Кiлькiсть м'ячiв
			<< balls.count << setw(19 - size_number(balls.count)) << "| "
			// Загальна вартiсть
			<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;
	}
	//SetConsoleOutputCP(65001);

	cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+";

	ifs.close();

	SetConsoleCP(1251);
}

void edit_record()
{
	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		getchar();
		return;
	}
	else if (file_is_empty(FILE_NAME) == true)
	{
		cout << u8"Файл порожнiй!" << endl;
		getchar();
		return;
	}

	Balls balls; // Редактируемая запись
	char name[MAX_MARKA_SIZE]; // Искомое имя записи
	int answer(0), // Ответ пользователя
		count; // Счётчик
	bool record_is_exist(false), // Существование записи
		something_is_edit(false), // Отредактировать чтонибудь ещё
		research_record(false), // Повторный поиск записи
		full_search(false); // Разширеный поиск (с учётом реестра) или части имени

	do {

		ifstream ifs(FILE_NAME, ios_base::binary);
		count = 0;
		record_is_exist = false;
		full_search = false;

		do
		{
			cout << u8"1. За маркою" << endl
				<< u8"2. За номером в списку" << endl
				<< u8"Ваш вибiр: ";
			input_check(answer, "Error: Ваш вибiр: ");
		} while (answer < 1 || answer > 2);

		if (answer == 1)
		{
			do
			{
				do
				{
					cout << u8"Введiть марку м'яча: ";
					cin.getline(name, MAX_MARKA_SIZE);
				} while (strlen(name) == 0);

				// Предварительный просмотр
				cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
					<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
					<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

				// Поиск записи
				while (ifs.read((char*)&balls, sizeof(balls)))
					if (strcmp(balls.marka, name) == 0)
					{
						// №
						cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << ++count << setw(6 - size_number((count))) << "| "
							// Тип 
							<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
							// Матерiал
							<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
							// Марка
							<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
							// Країна виробник
							<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
							// Вартiсть за одиницю
							<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
							// Кiлькiсть м'ячiв
							<< balls.count << setw(19 - size_number(balls.count)) << "| "
							// Загальна вартiсть
							<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

						record_is_exist = true;
					}

				if (record_is_exist == false)
				{
					// Указатель чтения на начало файла
					count = 0;
					ifs.clear();
					ifs.seekg(0, ios_base::beg);

					// Разшириный поиск, по части имени или с другим реестром
					while (ifs.read((char*)&balls, sizeof(balls)))
						if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL)
						{
							// №
							cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << ++count << setw(6 - size_number((count))) << "| "
								// Тип 
								<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
								// Матерiал
								<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
								// Марка
								<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
								// Країна виробник
								<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
								// Вартiсть за одиницю
								<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
								// Кiлькiсть м'ячiв
								<< balls.count << setw(19 - size_number(balls.count)) << "| "
								// Загальна вартiсть
								<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

							record_is_exist = true;
						}
				}

				cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

				if (record_is_exist == false)
				{
					cout << u8"Запис не знайденно!" << endl
						<< u8"Спробувати ще [y/n]?: ";
					if (answer_check() == true)
					{
						ifs.close();
						system("cls");
						edit_record();
						return;
					}
				}

			} while (record_is_exist == false);

			ifs.clear(); // Чтение сначала файла
			ifs.seekg(0, ios_base::beg);

			// Если найденно записей больше одной, предоставляем выбор
			if (count > 1)
			{
				do
				{
					cout << u8"Введiть номер запису для редагування: ";
					input_check(answer, "Error: Ведiть номер запису для редагування: ");
				} while (answer < 1 || answer > count); // Проверка на пренадлежность границам вывидиных записей

				count = 0;
				if (full_search == true) // Розширеный поиск, на выходе будет искомая запись
				{
					while (ifs.read((char*)&balls, sizeof(balls)))
						if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL)
							if ((--answer) == 0) break;
							else count++; // Индекс искомой структуры
				}
				else // Поиск с проверкой полного совпадения имени
				{
					while (ifs.read((char*)&balls, sizeof(balls)))
						if (strcmp(balls.marka, name) == 0)
							if ((--answer) == 0) break;
							else count++; // Индекс искомой структуры
				}

				// FIXME: Выводит номер записи 0
				cout << endl << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
					<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
					<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
					<< fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << answer + 1 << setw(6 - size_number((answer))) << "| "
					// Тип 
					<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
					// Матерiал
					<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
					// Марка
					<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
					// Країна виробник
					<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
					// Вартiсть за одиницю
					<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
					// Кiлькiсть м'ячiв
					<< balls.count << setw(19 - size_number(balls.count)) << "| "
					// Загальна вартiсть
					<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl
					<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;
			}
			else // Если одна, тогда без выбора
			{
				count = 0;
				if (full_search == true) // Розширеный поиск, на выходе будет искомая запись
				{
					while (ifs.read((char*)&balls, sizeof(balls)))
						if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL) break;
						else count++; // Индекс искомой структуры
				}
				else // Поиск с проверкой полного совпадения имени
				{
					while (ifs.read((char*)&balls, sizeof(balls)))
						if (strcmp(balls.marka, name) == 0) break;
						else count++; // Индекс искомой структуры
				}
			}

			ifs.close();
		}
		else
		{
			show_all_records();

			if (num_entries_file() != 0)
			{
				do
				{
					cout << endl << u8"Введiть номер запису: ";
					input_check(answer, "Error: Введiть номер запису: ");
				} while (answer < 0 || answer > num_entries_file());

				while (ifs.read((char*)&balls, sizeof(balls)))
					if ((--answer) == 0) break;
					else count++; // Индекс искомой структуры
			}
		}

		// Редактирование
		do
		{
			something_is_edit = false;

			cout << endl << u8"Що редагувати?" << endl
				<< u8"1. Все" << endl
				<< u8"2. Тип" << endl
				<< u8"3. Матерiал" << endl
				<< u8"4. Марка" << endl
				<< u8"5. Країна виробник" << endl
				<< u8"6. Вартiсть за одиницю" << endl
				<< u8"7. Кiлькiсть м'ячiв" << endl
				//<< u8"8. Загальна вартiсть" << endl
				<< u8"8. Повернутись в головне меню" << endl
				<< u8"0. Знайти iнший запис" << endl
				<< u8"Ваш вибiр: ";
			input_check(answer, "Error: Ваш вибiр: ");

			switch (answer)
			{
			case 1: // Все
			{
				// Ввод типа мяча
				do
				{
					cout << endl << u8"Вкажiть тип мяча: " << endl
						<< u8"1. Футбольний" << endl
						<< u8"2. Волейбольний" << endl
						<< u8"3. Баскетбольний" << endl
						<< u8"Ваш вибiр: ";
					input_check(balls.type, "Ваш вибiр: ");
				} while (balls.type < FOOTBALL || balls.type > BASKETBALL); // Проверка на пренадлежность с зарезервироваными типами

				// Ввод материала мяча
				do
				{
					cout << endl << u8"Вкажiть матерiал м'яча: " << endl
						<< u8"1. Синтетична шкiра" << endl
						<< u8"2. Штучна шкiра" << endl
						<< u8"3. Композитна шкiра" << endl
						<< u8"4. Натуральна шкiра" << endl
						<< u8"Ваш вибiр: ";
					input_check(balls.material, "Ваш вибiр: ");
				} while (balls.material < SYNTHETIC || balls.material > NATURAL); // Проверка на пренадлежность с зарезервироваными материалами

				// Ввод имени мяча
				do
				{
					cout << endl << u8"Марка м'яча (макс. 10 символiв): ";
					cin.getline(balls.marka, MAX_MARKA_SIZE);
				} while (strlen(balls.marka) == 0); // Имя не может быть пустым

				// Ввод страны производителя мяча
				do
				{
					cout << endl << u8"Вкажiть країну виробника м'яча: " << endl
						<< u8"1. Україна" << endl
						<< u8"2. США" << endl
						<< u8"3. Китай" << endl
						<< u8"4. Японiя" << endl
						<< u8"5. Данiя" << endl
						<< u8"6. Iспанiя" << endl
						<< u8"Ваш вибiр: ";
					input_check(balls.country, "Ваш вибiр: ");
				} while (balls.country < UKRAINE || balls.country > SPAIN); // Проверка на пренадлежность с зарезервироваными странам

				// Ввод цены мяча
				do
				{
					cout << endl << u8"Вартiсть м'яча: ";
					input_check(balls.price, "Вартiсть м'яча: ");
				} while (balls.price <= 0);

				// Ввод количества мячей
				do
				{
					cout << endl << u8"Кiлькiсть м'ячiв: ";
					input_check(balls.count, "Кiлькiсть м'ячiв: ");
				} while (balls.count <= 0);

				// Общая стоимость мячей
				balls.all_price = balls.price * balls.count;
				//cout << endl << u8"Автоматично вирахувана загальна вартiсть ( " << balls.all_price << u8" )"/*, правильно [y/n]?: "*/;
				//if (answer_check() == false)
				//{
				//	do
				//	{
				//		cout << endl << u8"Загальна вартiсть м'ячiв: ";
				//		input_check(balls.all_price, "Загальна вартiсть м'ячiв: ");
				//	} while (balls.all_price <= 0);
				//}

				something_is_edit = true;
				break;
			}
			case 2: // Тип
			{
				// Ввод типа мяча
				do
				{
					cout << endl << u8"Вкажiть тип мяча: " << endl
						<< u8"1. Футбольний" << endl
						<< u8"2. Волейбольний" << endl
						<< u8"3. Баскетбольний" << endl
						<< u8"Ваш вибiр: ";
					input_check(balls.type, "Ваш вибiр: ");
				} while (balls.type < FOOTBALL || balls.type > BASKETBALL); // Проверка на пренадлежность с зарезервироваными типам

				something_is_edit = true;
				break;
			}
			case 3: // Матерiал
			{
				// Ввод материала мяча
				do
				{
					cout << endl << u8"Вкажiть матерiал м'яча: " << endl
						<< u8"1. Синтетична шкiра" << endl
						<< u8"2. Штучна шкiра" << endl
						<< u8"3. Композитна шкiра" << endl
						<< u8"4. Натуральна шкiра" << endl
						<< u8"Ваш вибiр: ";
					input_check(balls.material, "Ваш вибiр: ");
				} while (balls.material < SYNTHETIC || balls.material > NATURAL); // Проверка на пренадлежность с зарезервироваными материалам

				something_is_edit = true;
				break;
			}
			case 4: // Марка
			{
				// Ввод имени мяча
				do
				{
					cout << endl << u8"Марка м'яча (макс. 10 символiв): ";
					cin.getline(balls.marka, MAX_MARKA_SIZE);
				} while (strlen(balls.marka) == 0);

				something_is_edit = true;
				break;
			}
			case 5: // Країна виробник
			{
				// Ввод страны производителя мяча
				do
				{
					cout << endl << u8"Вкажiть країну виробника м'яча: " << endl
						<< u8"1. Україна" << endl
						<< u8"2. США" << endl
						<< u8"3. Китай" << endl
						<< u8"4. Японiя" << endl
						<< u8"5. Данiя" << endl
						<< u8"6. Iспанiя" << endl
						<< u8"Ваш вибiр: ";
					input_check(balls.country, "Ваш вибiр: ");
				} while (balls.country < UKRAINE || balls.country > SPAIN); // Проверка на пренадлежность с зарезервироваными странами

				something_is_edit = true;
				break;
			}
			case 6: // Вартiсть за одиницю
			{
				// Ввод цены мяча
				do
				{
					cout << endl << u8"Вартiсть м'яча: ";
					input_check(balls.price, "Вартiсть м'яча: ");
				} while (balls.price <= 0);

				something_is_edit = true;
			}
			//case 8: // Загальна вартiсть
			//{
			//	// Общая стоимость мячей
			//	balls.all_price = balls.price * balls.count;
			//	cout << endl << u8"Автоматично вирахувана загальна вартiсть ( " << balls.all_price << u8" )"/*, правильно [y/n]?: "*/;
			//	//if (answer_check() == false)
			//	//{
			//	//	do
			//	//	{
			//	//		cout << endl << u8"Загальна вартiсть м'ячiв: ";
			//	//		input_check(balls.all_price, "Загальна вартiсть м'ячiв: ");
			//	//	} while (balls.all_price <= 0);
			//	//}


			//	something_is_edit = true;
			//	break;
			//}
			case 7: // Кiлькiсть м'ячiв
			{
				// Ввод количества мячей
				do
				{
					cout << endl << u8"Кiлькiсть м'ячiв: ";
					input_check(balls.count, "Кiлькiсть м'ячiв: ");
				} while (balls.count <= 0);

				// Общая стоимость мячей
				balls.all_price = balls.price * balls.count;
				//cout << endl << u8"Автоматично вирахувана загальна вартiсть ( " << balls.all_price << u8" )"/*, правильно [y/n]?: "*/;
				//if (answer_check() == false)
				//{
				//	do
				//	{
				//		cout << endl << u8"Загальна вартiсть м'ячiв: ";
				//		input_check(balls.all_price, "Загальна вартiсть м'ячiв: ");
				//	} while (balls.all_price <= 0);
				//}


				something_is_edit = true;
				break;
			}
			case 8: // Повернутись в головне меню
				return;
				break;
			}

			// Если пользователь что-то отредактировал, тогда обновляем структуру в фале.
			if (something_is_edit == true)
			{
				// Запись изменений в файл.
				fstream fs(FILE_NAME, ios_base::binary | ios_base::in | ios_base::out /*открытие для чтения и записи*/);
				fs.seekp((sizeof(balls)*count), ios_base::beg);
				fs.write((char*)&balls, sizeof(balls));
				fs.close();

				system("cls");
				show_all_records();
				cout << u8"Вiдредагувати ще [y/N]?: ";
				something_is_edit = answer_check();
			}

		} while (something_is_edit == true);

	} while (answer == 0);
}

void delete_record()
{
	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		getchar();
		return;
	}
	else if (file_is_empty(FILE_NAME) == true)
	{
		cout << u8"Файл порожнiй!" << endl;
		getchar();
		return;
	}

	Balls balls;
	char name[MAX_MARKA_SIZE]; // Искомое имя записи
	int answer(0), // Ответ пользователя
		count; // Счётчик
	bool record_is_exist(false), // Существование записи
		something_is_del(false),// Отредактировать чтонибудь ещё
		full_search(false); // Расширеный поиск (с учётом реестра) и часичного имени записи

	ifstream ifs(FILE_NAME, ios_base::binary); // Исходный файл для чтения всех записей

	do
	{
		cout << u8"1. За маркою" << endl
			<< u8"2. За номером в списку" << endl
			<< u8"Ваш вибiр: ";
		input_check(answer, "Error: Ваш вибiр: ");
	} while (answer < 1 || answer > 2);

	if (answer == 1)
	{

		do
		{
			count = 0;

			do
			{
				cout << u8"Введiть марку м'яча: ";
				cin.getline(name, MAX_MARKA_SIZE);
			} while (strlen(name) == 0);

			// Предварительный просмотр
			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
				<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
				<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			// Поиск записи
			while (ifs.read((char*)&balls, sizeof(balls)))
				if (strcmp(balls.marka, name) == 0)
				{
					// №
					cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << ++count << setw(6 - size_number((count))) << "| "
						// Тип 
						<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
						// Матерiал
						<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
						// Марка
						<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
						// Країна виробник
						<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
						// Вартiсть за одиницю
						<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
						// Кiлькiсть м'ячiв
						<< balls.count << setw(19 - size_number(balls.count)) << "| "
						// Загальна вартiсть
						<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

					record_is_exist = true;
				}

			if (record_is_exist == false)
			{
				count = 0;

				// Указатель чтения на начало файла
				ifs.clear();
				ifs.seekg(0, ios_base::beg);

				// Разшириный поиск, по части имени или с другим реестром
				while (ifs.read((char*)&balls, sizeof(balls)))
					if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL)
					{
						// №
						cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << ++count << setw(6 - size_number((count))) << "| "
							// Тип 
							<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
							// Матерiал
							<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
							// Марка
							<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
							// Країна виробник
							<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
							// Вартiсть за одиницю
							<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
							// Кiлькiсть м'ячiв
							<< balls.count << setw(19 - size_number(balls.count)) << "| "
							// Загальна вартiсть
							<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

						record_is_exist = true;
						full_search = true;
					}
			}

			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			if (record_is_exist == false)
			{
				cout << u8"Запис не знайденно!\nСпробувати ще [y/N]?: ";
				if (answer_check() == true)
				{
					ifs.close();
					system("cls");
					delete_record();
					return;
				}
			}

		} while (record_is_exist == false);

		ifs.clear(); // Чтение сначала файла
		ifs.seekg(0, ios_base::beg);

		// Если найденно записей больше одной, предоставляем выбор
		if (count > 1)
		{
			do
			{
				cout << u8"Введiть номер запису для удаления: ";
				input_check(answer, "Error: Ведiть номер запису для удаления: ");
			} while (answer < 1 || answer > count);

			count = 1;
			if (full_search == true) // расширеный поиск (с учётом реестра) и частичного имени 
			{
				while (ifs.read((char*)&balls, sizeof(balls)))
					if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL)
						if ((--answer) == 0) break;
						else count++;
			}
			else // поиск строго по имени 
			{
				while (ifs.read((char*)&balls, sizeof(balls)))
					if (strcmp(balls.marka, name) == 0)
						if ((--answer) == 0) break;
						else count++;
			}

			cout << endl << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
				<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
				<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
				<< fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << answer << setw(6 - size_number((answer))) << "| "
				// Тип 
				<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
				// Матерiал
				<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
				// Марка
				<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
				// Країна виробник
				<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
				// Вартiсть за одиницю
				<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
				// Кiлькiсть м'ячiв
				<< balls.count << setw(19 - size_number(balls.count)) << "| "
				// Загальна вартiсть
				<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl
				<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

		}
		else // Если одна, тогда без выбора
		{
			count = 1;
			if (full_search == true)
			{
				while (ifs.read((char*)&balls, sizeof(balls))) // расширеный поиск (с учётом реестра) и частичного имени 
					if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL) break;
					else count++;
			}
			else
			{
				while (ifs.read((char*)&balls, sizeof(balls)))
					if (strcmp(balls.marka, name) == 0) break;
					else count++;  // поиск строго по имени 
			}
		}

		ifs.close();
	}
	else
	{
		show_all_records();
		do
		{
			cout << endl << u8"Введiть номер запису: ";
			input_check(count, "Error: Введiть номер запису: ");
		} while (count < 0 || count > num_entries_file());
	}

	cout << u8"Видалити запис [y/N]?:";
	if (answer_check() == true)
	{
		ofstream ofs(FILE_TMP, ios_base::binary | ios_base::trunc); // Временый файл
		ifstream ifs(FILE_NAME, ios_base::binary); // Исходный файл

		// Запись всех записей кроме удаляемой, в временый файл
		while (ifs.read((char*)&balls, sizeof(Balls)))
			if ((--count) != 0) ofs.write((char*)&balls, sizeof(Balls));

		ifs.close();
		ofs.close();

		ofs.open(FILE_NAME, ios_base::binary | ios_base::trunc); // Исходный файл
		ifs.open(FILE_TMP, ios_base::binary); // Временый файл
		ofs << ifs.rdbuf(); // Копирование содержимого временого файла в исходый файл
		ifs.close();
		ofs.close();

		system("cls");
		show_all_records();
	}

	cout << u8"Видалити iнший запис [y/N]?: ";
	if (answer_check() == true)
	{
		system("cls");
		delete_record();
	}
}

void sorting_records()
{
	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		getchar();
		return;
	}
	else if (file_is_empty(FILE_NAME) == true)
	{
		cout << u8"Файл порожнiй!" << endl;
		getchar();
		return;
	}

	Balls *tmp = new Balls[2]; // Сравниваемые структуры
	int answer(0), // Ответ пользователя
		size = sizeof(tmp[0]); // Размер одной структуры

	fstream fs(FILE_NAME, ios_base::binary | ios_base::in | ios_base::out /*открытие для чтения и записи*/);

	do
	{
		system("cls");
		do
		{
			cout << u8"За якою ознакою вiдсортувати мя'чi: " << endl
				<< u8"1. Марка (зменшення)" << endl
				<< u8"2. Марка (збiльшення)" << endl
				<< u8"3. Вартiсть за одиницю (зменшення)" << endl
				<< u8"4. Вартiсть за одиницю (збiльшення)" << endl
				<< u8"5. Повернутись в головне меню" << endl
				<< u8"Ваш вибiр: ";
			input_check(answer, "Error: Ваш вибiр: ");
		} while (answer < 1 || answer > 5);

		// Сортировка пузырька
		switch (answer)
		{
		case 1: // Марка (зменшення)
		{
			while (fs.read((char *)tmp, size * 2))
				if (strcmp(tmp[0].marka, tmp[1].marka) < 0)
				{
					fs.seekp(size * (-2), std::ios_base::cur);

					fs.write((char *)&tmp[1], size);
					fs.write((char *)&tmp[0], size);

					fs.seekp(0, std::ios_base::beg);
				}
				else
					fs.seekp(-size, std::ios_base::cur);
			break;
		}
		case 2: // Марка (збiльшення)
		{
			while (fs.read((char *)tmp, size * 2))
				if (strcmp(tmp[0].marka, tmp[1].marka) > 0)
				{
					fs.seekp(size * (-2), std::ios_base::cur);

					fs.write((char *)&tmp[1], size);
					fs.write((char *)&tmp[0], size);

					fs.seekp(0, std::ios_base::beg);
				}
				else
					fs.seekp(-size, std::ios_base::cur);

			break;
		}
		case 3: // Вартiсть за одиницю (зменшення)
		{	while (fs.read((char *)tmp, size * 2))
			if (tmp[0].price < tmp[1].price)
			{
				fs.seekp(size * (-2), std::ios_base::cur);

				fs.write((char *)&tmp[1], size);
				fs.write((char *)&tmp[0], size);

				fs.seekp(0, std::ios_base::beg);
			}
			else
				fs.seekp(-size, std::ios_base::cur);

		break;
		}
		case 4: // Вартiсть за одиницю (збiльшення)
		{	while (fs.read((char *)tmp, size * 2))
			if (tmp[0].price > tmp[1].price)
			{
				fs.seekp(size * (-2), std::ios_base::cur);

				fs.write((char *)&tmp[1], size);
				fs.write((char *)&tmp[0], size);

				fs.seekp(0, std::ios_base::beg);
			}
			else
				fs.seekp(-size, std::ios_base::cur);

		break;
		}
		case 5: // Повернутись в головне меню
		{	return;
		break;
		}
		default:
			break;
		}

		show_all_records();
		getchar();

	} while (true);

	delete[] tmp;
	fs.close();
}

void request()
{
	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		getchar();
		return;
	}
	else if (file_is_empty(FILE_NAME) == true)
	{
		cout << u8"Файл порожнiй!" << endl;
		getchar();
		return;
	}

	int answer(0); // ответ пользователя
	Balls balls; // искомая структура

	ifstream ifs(FILE_NAME, ios_base::binary);

	do
	{
		system("cls");
		ifs.clear(); // Указатель на начало файла
		ifs.seekg(0, ios_base::beg);

		cout << u8"Який запит виконати: " << endl
			<< u8"1. Загальна вартiсть продажу м'ячiв обраного типу" << endl
			<< u8"2. Найдорожчий м'яч обраного типу матерiалу" << endl
			<< u8"3. Найпопулярнiшi м'ячi кожного з типiв" << endl
			<< u8"4. Перелiк проданих м'ячiв, вартiсть (за одиницю) яких у введеному промiжку цiн" << endl
			<< u8"5. Вiдсоткове спiввiдношення продажу м'ячiв за усiма типами шкiр" << endl
			<< u8"6. В головне меню" << endl
			<< u8"Ваш вибiр: ";
		input_check(answer, "Error: Ваш вибiр: ");

		switch (answer)
		{
		case 1: // Загальна вартiсть продажу м'ячiв обраного типу
		{
			bool type_is_exist(false);
			int type(0); // Тип мяча
			float sum_all_prices(0); // Общая стоимость всех заказов витраного типа мяча

			// Ввод типа мяча
			do
			{
				cout << endl << u8"Вкажiть тип мяча: " << endl
					<< u8"1. Футбольний" << endl
					<< u8"2. Волейбольний" << endl
					<< u8"3. Баскетбольний" << endl
					//<< u8"4. Iнший запит" << endl
					<< u8"Ваш вибiр: ";
				input_check(type, "Ваш вибiр: ");
			} while (type < FOOTBALL || type > BASKETBALL); // проверка на пренадлежность с зарезервироваными типами

			while (ifs.read((char*)&balls, sizeof(balls))) // подсчет общей стоимости
				if (balls.type == type)
				{
					sum_all_prices += balls.all_price;
					type_is_exist = true;
				}

			cout << endl << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << u8"Загальна вартiсть продажу м'ячiв типу \"" << (type == FOOTBALL ? "Футбольний" :
				type == VOLLEYBALL ? u8"Волейбольний" : "Баскетбольний") << u8"\" = ";

			if (type_is_exist == true) cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << sum_all_prices << endl;
			else cout << u8"Жодного м'яча не продано!" << endl;

			break;
		}
		case 2: // Найдорожчий м'яч обраного типу матерiалу
		{
			bool record_is_exist(false); // Проверка на существование записи
			int material(0); // Тип мяча
			float max_price(0.0f); // Общая стоимость всех заказов витраного типа мяча

			// Ввод материала мяча
			do
			{
				cout << endl << u8"Вкажiть матерiал м'яча: " << endl
					<< u8"1. Синтетична шкiра" << endl
					<< u8"2. Штучна шкiра" << endl
					<< u8"3. Композитна шкiра" << endl
					<< u8"4. Натуральна шкiра" << endl
					<< u8"Ваш вибiр: ";
				input_check(material, "Ваш вибiр: ");
			} while (material < SYNTHETIC || material > NATURAL); // Проверка на пренадлежность с зарезервироваными материалами

			while (ifs.read((char*)&balls, sizeof(balls))) // подсчёт максимальной стоимости
				if ((balls.material == material) && (balls.price > max_price))
				{
					max_price = balls.price;
					record_is_exist = true;
				}

			cout << endl << u8"Найдорожчий м'яч обраного типу матерiалу \"" << (material == SYNTHETIC ?
				"Синтетична шкiра" : material == ARTIFICIAL ?
				"Штучна шкiра" : material == COMPOSITE ?
				"Композитна шкiра" : "Натуральна шкiра")
				<< u8"\" = ";

			if (record_is_exist == true) cout << max_price << endl;
			else cout << u8"Жодного м'яча даного типу, не проданно!" << endl;

			break;
		}
		case 3: // Найпопулярнiшi м'ячi кожного з типiв
		{
			bool is_exist; // Поможет при нахождение уникальных записей
			int count(0); // Граница масива
			Balls* ballArray = new Balls[num_entries_file()]; // Масив с иникальными именами в файле
			Balls ballPopular[3]; // популярные записи

			while (ifs.read((char*)&balls, sizeof(balls)))
			{
				is_exist = false;
				for (int i(0); i < count; i++) // Проперка на уникальность
					if (strcmp(ballArray[i].marka, balls.marka) == 0 && ballArray[i].type != balls.type)
					{
						//ballArray[i].count += balls.count;
						//ballArray[i].all_price += balls.all_price;
						is_exist = true;
					}

				if (is_exist == false)
				{
					strcpy_s(ballArray[count++].marka, MAX_MARKA_SIZE, balls.marka);
					ballArray[count - 1].type = balls.type;
					ballArray[count - 1].material = balls.material;
					ballArray[count - 1].price = balls.price;
					ballArray[count - 1].country = balls.country;
				}
			}

			ifs.clear();
			ifs.seekg(0, ios_base::beg);

			while (ifs.read((char*)&balls, sizeof(balls)))
				for (int i(0); i < count; i++)
					if (strcmp(ballArray[i].marka, balls.marka) == 0 && ballArray[i].type == balls.type)
					{
						ballArray[i].count += balls.count;
						ballArray[i].all_price += balls.all_price;
					}

			for (int i = 0; i < count; i++) // поиск самой популярной записи по каждому типу
			{
				switch (ballArray[i].type)
				{
				case FOOTBALL:
					if (ballPopular[0].count < ballArray[i].count)
						ballPopular[0] = ballArray[i];
					break;
				case VOLLEYBALL:
					if (ballPopular[1].count < ballArray[i].count)
						ballPopular[1] = ballArray[i];
					break;
				case BASKETBALL:
					if (ballPopular[2].count < ballArray[i].count)
						ballPopular[2] = ballArray[i];
					break;
				default:
					break;
				}
			}

			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
				<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
				<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			for (size_t i(0); i < 3; i++)
			{
				if (ballPopular[i].count == 0) continue;

				// №
				cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << i + 1 << setw(6 - size_number((i + 1))) << "| "
					// Тип 
					<< (ballPopular[i].type == FOOTBALL ? "Футбольний   " : ballPopular[i].type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
					// Матерiал
					<< (ballPopular[i].material == SYNTHETIC ? u8"Синтетична шкiра " : ballPopular[i].material == ARTIFICIAL ? u8"Штучна шкiра     " : ballPopular[i].material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
					// Марка
					<< ballPopular[i].marka << setw(13 - strlen(ballPopular[i].marka)) << "| "
					// Країна виробник
					<< (ballPopular[i].country == UKRAINE ? u8"Україна" : ballPopular[i].country == USA ? u8"США    " : ballPopular[i].country == CHINA ? u8"Китай  " : ballPopular[i].country == JAPAN ? u8"Японiя " : ballPopular[i].country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
					// Вартiсть за одиницю
					<< ballPopular[i].price << setw(22 - size_number(ballPopular[i].price)) << u8" | "
					// Кiлькiсть м'ячiв
					<< ballPopular[i].count << setw(19 - size_number(ballPopular[i].count)) << "| "
					// Загальна вартiсть
					<< ballPopular[i].all_price << setw(19 - size_number(ballPopular[i].all_price)) << u8"|" << endl;
			}

			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			//cout << u8"Найпопулярнiшi м'ячi типiв:" << endl;

			//cout << "Футбольний    -> ";
			//if (football.count == 0) cout << u8"Не продано жодного м'яча даного типу!" << endl;
			//else cout << football.marka << u8", кiлькiть проданих м'ячiв = " << football.count << endl;
			//cout << u8"Волейбольний  -> ";
			//if (volleyball.count == 0) cout << u8"Не продано жодного м'яча даного типу!" << endl;
			//else cout << volleyball.marka << u8", кiлькiть проданих м'ячiв = " << volleyball.count << endl;
			//cout << u8"Баскетбольний -> ";
			//if (basketball.count == 0) cout << u8"Не продано жодного м'яча даного типу!" << endl;
			//else cout << basketball.marka << u8", кiлькiть проданих м'ячiв = " << basketball.count << endl;

			delete[] ballArray;
			break;
		}
		case 4: // Перелiк проданих м'ячiв, вартiсть (за одиницю) яких у введеному промiжку цiн
		{
			bool record_between_broads_is_exist(false); // Проверка на вхождение записи в диапазон
			int count(1); // Счётчик
			float broad_one, broad_two; // Границы

			do
			{
				cout << u8"Введiть першу границю промiжку цiни: ";
				input_check(broad_one, "Error: Введiть першу границю промiжку цiни: ");
			} while (broad_one < 0);

			do
			{
				cout << u8"Введiть другу границю промiжку цiни: ";
				input_check(broad_two, "Error: Введiть другу границю промiжку цiни: ");
			} while (broad_two < 0);

			if (broad_one > broad_two) // переворот границ, для упрощения поиска
				swap(broad_one, broad_two);

			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
				<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
				<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			while (ifs.read((char*)&balls, sizeof(balls)))
			{
				if (balls.price >= broad_one && balls.price <= broad_two) // Проверка на пренадлежность границам
				{
					// №
					cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << count++ << setw(6 - size_number((count - 1))) << "| "
						// Тип 
						<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
						// Матерiал
						<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
						// Марка
						<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
						// Країна виробник
						<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
						// Вартiсть за одиницю
						<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
						// Кiлькiсть м'ячiв
						<< balls.count << setw(19 - size_number(balls.count)) << "| "
						// Загальна вартiсть
						<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

					record_between_broads_is_exist = true;
				}
			}

			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			if (record_between_broads_is_exist == false)
				cout << u8"М'ячiв з цiною в промiжку вiд \"" << broad_one << u8"\" до \"" << broad_one << u8"\" немає! " << endl;

			break;
		}
		case 5: // Вiдсоткове спiввiдношення продажу м'ячiв за усiма типами шкiр
		{
			int synthetic(0), // Тип шкуры -  синтетична
				artificial(0), // Тип шкуры -  штучна 
				composite(0), // Тип шкуры -  композытна 
				natural(0), // Тип шкуры -  натуральна
				count; // Счётчик

			float synthetic_procent(0.0f), // Процентное соотношение - синтетична
				artificial_procent(0.0f), // Процентное соотношение -  штучна 
				composite_procent(0.0f), // Процентное соотношение - композытна 
				natural_procent(0.0f); // Процентное соотношение -  натуральна

			while (ifs.read((char*)&balls, sizeof(balls)))
				switch (balls.material)
				{
				case SYNTHETIC:
					synthetic++;
					break;
				case ARTIFICIAL:
					artificial++;
					break;
				case COMPOSITE:
					composite++;
					break;
				case NATURAL:
					natural++;
					break;
				default:
					break;
				}

			count = synthetic + artificial + composite + natural;
			synthetic_procent = ((float)synthetic / count) * 100;
			artificial_procent = ((float)artificial / count) * 100;
			composite_procent = ((float)composite / count) * 100;
			natural_procent = ((float)natural / count) * 100;

			cout << fixed << setprecision(1) /*вывод с 1 знакoм после запятой*/
				<< u8"+══════════════════+════════════+═════════--+" << endl
				<< u8"|     Тип шкiри    |  Кiлькiсть |     %     |" << endl
				<< u8"+══════════════════+════════════+═════════--+" << endl
				<< u8"| Синтетична шкiра | " << synthetic << setw(13 - size_number(synthetic)) << u8" | " << synthetic_procent << '%' << setw(12 - size_number(synthetic_procent)) << u8" | " << endl
				<< u8"| Штучна шкiра	   | " << artificial << setw(13 - size_number(artificial)) << u8" | " << artificial_procent << '%' << setw(12 - size_number(artificial_procent)) << u8" | " << endl
				<< u8"| Композитна шкiра | " << composite << setw(13 - size_number(composite)) << u8" | " << composite_procent << '%' << setw(12 - size_number(composite_procent)) << u8" | " << endl
				<< u8"| Натуральна шкiра | " << natural << setw(13 - size_number(natural)) << u8" | " << natural_procent << '%' << setw(12 - size_number(natural_procent)) << u8" | " << endl
				<< u8"+══════════════════+════════════+═════════--+" << endl;

			break;
		}
		case 6: // В головне меню
		{
			ifs.close();
			return;
		}
		default:
			break;
		}
		getchar();
	} while (true);
}

void find()
{
	if (file_is_exists(FILE_NAME) == false)
	{
		cout << u8"Файл не iснує!" << endl;
		getchar();
		return;
	}
	else if (file_is_empty(FILE_NAME) == true)
	{
		cout << u8"Файл порожнiй!" << endl;
		getchar();
		return;
	}

	Balls balls;
	int count(1); // Счётчик для первого столбца талбицы
	char name[MAX_MARKA_SIZE];
	bool full_search(false),
		record_is_exist(false); // Существование записи

	ifstream ifs(FILE_NAME, ios_base::binary);

	do
	{
		ifs.clear();
		ifs.seekg(0, ios_base::beg);

		full_search = false;
		record_is_exist = false;

		do
		{
			count = 0;

			do
			{
				cout << u8"Введiть марку м'яча: ";
				cin.getline(name, MAX_MARKA_SIZE);
			} while (strlen(name) == 0);

			// Предварительный просмотр
			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl
				<< u8"|  №  |      Тип      |     Матерiал     |   Марка    | Країна виробник | Вартiсть за одиницю | Кiлькiсть м'ячiв | Загальна вартiсть |" << endl
				<< u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			// Поиск записи
			while (ifs.read((char*)&balls, sizeof(balls)))
				if (strcmp(balls.marka, name) == 0)
				{
					// №
					cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << ++count << setw(6 - size_number((count))) << "| "
						// Тип 
						<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
						// Матерiал
						<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
						// Марка
						<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
						// Країна виробник
						<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
						// Вартiсть за одиницю
						<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
						// Кiлькiсть м'ячiв
						<< balls.count << setw(19 - size_number(balls.count)) << "| "
						// Загальна вартiсть
						<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

					record_is_exist = true;
				}

			if (record_is_exist == false)
			{
				count = 0;
				// Указатель чтения на начало файла
				ifs.clear();
				ifs.seekg(0, ios_base::beg);

				// Разшириный поиск, по части имени или с другим реестром
				while (ifs.read((char*)&balls, sizeof(balls)))
					if (strstr(str_tolower(balls.marka), str_tolower(name)) != NULL)
					{
						// №
						cout << fixed << setprecision(2) /*вывод с 2-мя знаками после запятой*/ << "| " << ++count << setw(6 - size_number((count))) << "| "
							// Тип 
							<< (balls.type == FOOTBALL ? "Футбольний   " : balls.type == VOLLEYBALL ? u8"Волейбольний " : "Баскетбольний") << u8" | "
							// Матерiал
							<< (balls.material == SYNTHETIC ? u8"Синтетична шкiра " : balls.material == ARTIFICIAL ? u8"Штучна шкiра     " : balls.material == COMPOSITE ? u8"Композитна шкiра " : "Натуральна шкiра ") << "| "
							// Марка
							<< balls.marka << setw(13 - strlen(balls.marka)) << "| "
							// Країна виробник
							<< (balls.country == UKRAINE ? u8"Україна" : balls.country == USA ? u8"США    " : balls.country == CHINA ? u8"Китай  " : balls.country == JAPAN ? u8"Японiя " : balls.country == DENMARK ? u8"Данiя  " : "Iспанiя") << u8"         | "
							// Вартiсть за одиницю
							<< balls.price << setw(22 - size_number(balls.price)) << u8" | "
							// Кiлькiсть м'ячiв
							<< balls.count << setw(19 - size_number(balls.count)) << "| "
							// Загальна вартiсть
							<< balls.all_price << setw(19 - size_number(balls.all_price)) << u8"|" << endl;

						record_is_exist = true;
						full_search = true;
					}
			}

			cout << u8"+═══--+═══════════════+══════════════════+════════════+═══════════════--+═════════════════════+══════════════════+══════════════════-+" << endl;

			if (record_is_exist == false)
			{
				cout << u8"Запис не знайденно!\nСпробувати ще [y/N]?: ";
				if (answer_check() == false)
				{
					ifs.close();
					return;
				}

				ifs.clear();
				ifs.seekg(0, ios_base::beg);
			}

		} while (record_is_exist == false);

		cout << u8"Знайти ще запис [y/N]?";
	} while (answer_check() == true);

	ifs.close();
}